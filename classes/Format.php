<?php

class Format
{

    /**
     * @param $sPrice
     *
     * @return string
     */
    public static function showPrice ($sPrice)
    {

        $dPrice = round(ceil($sPrice * 100) / 100, 2);
        return number_format($dPrice, 2, ',', '.');
    }

}