<?php

class Database
{

	private $sServerName = 'nielsprins.com';

	private $sUserName = 'nielsprins_iprop';

	private $sPassword = 'Z$wq8uWfIXQpY9hZXts8FTDu!';

	private $sDataBaseName = 'nielsprins_iprop';

	private $bDatabaseLoaded = false;

	private $oDatabaseConnection;

	public function __construct()
	{

		$this->loadDatabase();
	}

	private function loadDatabase()
	{

		if ( ! $this->bDatabaseLoaded ) {

			$oDatabaseConnection = new mysqli( $this->sServerName, $this->sUserName, $this->sPassword, $this->sDataBaseName );
			if ( $oDatabaseConnection->connect_error ) {

				die( 'Connection failed: ' . $oDatabaseConnection->connect_error );
			}

			$this->bDatabaseLoaded     = true;
			$this->oDatabaseConnection = $oDatabaseConnection;
		}

	}

	/**
	 * @return mysqli
	 */
	public function getConnection()
	{
		return $this->oDatabaseConnection;
	}

	public static function generateUUID()
	{
		$sCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$sUUID       = '';

		for ( $i = 0; $i < 11; $i++ ) {
			/** @noinspection PhpUnhandledExceptionInspection */
			$sUUID .= $sCharacters[ random_int( 0, strlen( $sCharacters ) - 1 ) ];
		}

		return $sUUID;
	}

}