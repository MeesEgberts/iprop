<?php

class SupplierDao
{

	private static function queryOne( $sQuery )
	{

		$oDatabase = new Database();
		$oDbc      = $oDatabase->getConnection();
		$oResult   = $oDbc->query( $sQuery );

		return ( $oResult instanceof mysqli_result ? $oResult->fetch_assoc() : $oResult );
	}

	private static function queryAll( $sQuery )
	{
		$aReturn = [];

		$oDatabase = new Database();
		$oDbc      = $oDatabase->getConnection();
		$oResult   = $oDbc->query( $sQuery );

		while ( $oRow = $oResult->fetch_assoc() ) {
			$aReturn[] = $oRow;
		}

		return $aReturn;

	}

	public static function getOne( $sID )
	{
		$sSql = 'SELECT * FROM `suppliers` WHERE `ID` = "' . $sID . '"';
		return self::queryOne( $sSql );
	}

	public static function getAll()
	{
		$sSql = 'SELECT * FROM `suppliers`';
		return self::queryAll( $sSql );
	}

	public static function getProducts( $sSupplierID ) {
		$sSql = 'SELECT * FROM `products` WHERE `SupplierID` = "' . $sSupplierID . '"';
		return self::queryAll( $sSql );
	}

}