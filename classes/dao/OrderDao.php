<?php

class OrderDao
{

	private static function queryOne( $sQuery )
	{

		$oDatabase = new Database();
		$oDbc      = $oDatabase->getConnection();
		$oResult   = $oDbc->query( $sQuery );

		return ( $oResult instanceof mysqli_result ? $oResult->fetch_assoc() : $oResult );
	}

	private static function queryAll( $sQuery )
	{
		$aReturn = [];

		$oDatabase = new Database();
		$oDbc      = $oDatabase->getConnection();
		$oResult   = $oDbc->query( $sQuery );

		while ( $oRow = $oResult->fetch_assoc() ) {
			$aReturn[] = $oRow;
		}

		return $aReturn;

	}

	public static function getOne( $sID )
	{
		$sSql = 'SELECT * FROM `orders` WHERE `ID` = "' . $sID . '"';
		return self::queryOne( $sSql );
	}

	public static function getAll()
	{
		$sSql = 'SELECT * FROM `orders`';
		return self::queryAll( $sSql );
	}

	public static function create( $sProductID, $iQuantity, $sSupplierID, $sUserEmail )
	{
		$sID  = Database::generateUUID();
		$sSql = 'INSERT INTO `orders` (`ID`, `ProductID`, `Quantity`, `SupplierID`, `UserEmail`) VALUES ("' . $sID . '", "' . $sProductID . '", "' . $iQuantity . '", "' . $sSupplierID . '", "' . $sUserEmail . '")';
		self::queryOne( $sSql );
		ProductDao::removeStock( $sProductID, $iQuantity );
		return true;
	}

	public static function updateStatus( $sOrderID, $sStatus )
	{
		$sSql = 'UPDATE `orders` SET `Status` = "' . $sStatus . '" WHERE `orders`.`ID` = "' . $sOrderID . '";';
		return self::queryOne( $sSql );
	}

}