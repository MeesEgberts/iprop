<?php

class ProductDao
{

	private static function queryOne( $sQuery )
	{

		$oDatabase = new Database();
		$oDbc      = $oDatabase->getConnection();
		$oResult   = $oDbc->query( $sQuery );

		return ( $oResult instanceof mysqli_result ? $oResult->fetch_assoc() : $oResult );
	}

	private static function queryAll( $sQuery )
	{
		$aReturn = [];

		$oDatabase = new Database();
		$oDbc      = $oDatabase->getConnection();
		$oResult   = $oDbc->query( $sQuery );

		while ( $oRow = $oResult->fetch_assoc() ) {
			$aReturn[] = $oRow;
		}

		return $aReturn;

	}

	public static function getOne( $sID )
	{
		$sSql = 'SELECT * FROM `products` WHERE `ID` = "' . $sID . '"';
		return self::queryOne( $sSql );
	}

	public static function getAll()
	{
		$sSql = 'SELECT * FROM `products`';
		return self::queryAll( $sSql );
	}

	public static function removeStock( $sProductID, $iQuantity )
	{

		$aProduct     = self::getOne( $sProductID );
		$iNewQuantity = $aProduct[ 'Stock' ] - $iQuantity;

		$sSql = 'UPDATE `products` SET `Stock` = "' . $iNewQuantity . '" WHERE `products`.`ID` = "' . $sProductID . '";';
		return self::queryOne( $sSql );
	}

}