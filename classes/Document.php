<?php

class Document
{

	public static function getHeader()
	{
		return '
<!DOCTYPE html>
<html lang="nl">
<head>
	<title>Alibaba</title>
	<meta name="viewport" content="width=device-width, initial-scale=0.8">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.7/css/mdb.min.css" rel="stylesheet">
	<link href="/files/css/style.css" rel="stylesheet">
	<link href="/files/css/product.css" rel="stylesheet">
	<link rel="stylesheet" href="/files/css/shop.css">
	<link rel="shortcut icon" href="/files/img/favicon.ico" type="image/x-icon" />
</head>
<body>

<nav class="site-header sticky-top py-1">
	<div class="container d-flex flex-column flex-md-row justify-content-between">
		<a class="py-2">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="d-block mx-auto"><circle cx="12" cy="12" r="10"></circle><line x1="14.31" y1="8" x2="20.05" y2="17.94"></line><line x1="9.69" y1="8" x2="21.17" y2="8"></line><line x1="7.38" y1="12" x2="13.12" y2="2.06"></line><line x1="9.69" y1="16" x2="3.95" y2="6.06"></line><line x1="14.31" y1="16" x2="2.83" y2="16"></line><line x1="16.62" y1="12" x2="10.88" y2="21.94"></line></svg>
		</a>
		<a class="py-2 d-none d-md-inline-block" href="/home">Home</a>
		<a class="py-2 d-none d-md-inline-block" href="/leveranciers.php">Leveranciers</a>
		<a class="py-2 d-none d-md-inline-block" href="/producten.php">Producten</a>
		<a class="py-2 d-none d-md-inline-block" href="/beheer.php">Beheer</a>
	</div>
</nav>
    	';
	}

	public static function getFooter()
	{
		return '<footer class="container py-5">
	<div class="row">
		<div class="col-12 col-md">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="d-block mb-2"><circle cx="12" cy="12" r="10"></circle><line x1="14.31" y1="8" x2="20.05" y2="17.94"></line><line x1="9.69" y1="8" x2="21.17" y2="8"></line><line x1="7.38" y1="12" x2="13.12" y2="2.06"></line><line x1="9.69" y1="16" x2="3.95" y2="6.06"></line><line x1="14.31" y1="16" x2="2.83" y2="16"></line><line x1="16.62" y1="12" x2="10.88" y2="21.94"></line></svg>
			<small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
		</div>
		<div class="col-6 col-md">
			<h5>Home</h5>
			<ul class="list-unstyled text-small">
				<li><a class="text-muted" href="/">Home</a></li>
			</ul>
		</div>
		<div class="col-6 col-md">
			<h5>Leveranciers</h5>
			<ul class="list-unstyled text-small">
				<li><a class="text-muted" href="/leveranciers.php">Leveranciers</a></li>
			</ul>
		</div>
		<div class="col-6 col-md">
			<h5>Producten</h5>
			<ul class="list-unstyled text-small">
				<li><a class="text-muted" href="/producten.php">Producten</a></li>
			</ul>
		</div>
		<div class="col-6 col-md">
			<h5>Beheer</h5>
			<ul class="list-unstyled text-small">
				<li><a class="text-muted" href="/beheer.php">Beheer</a></li>
			</ul>
		</div>
	</div>
</footer>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.7/js/mdb.min.js"></script>
<script type="module" src="/files/js/script.js"></script>
<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
</body>
</html>';
	}

}