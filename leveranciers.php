<?php
include 'files/includes.php';

echo Document::getHeader();

$aLeveranciers = SupplierDao::getAll();

?>

<div class="container">
	<div class="row">
		<ul>
			<?php
				foreach ($aLeveranciers as $leverancier) {
					echo '<a href="/leverancier/'.$leverancier['ID'].'">
							<li>'.$leverancier['Name'].'</li>
						</a>';
				}
			?>
		</ul>
	</div>
</div>

<?php
echo Document::getFooter();
