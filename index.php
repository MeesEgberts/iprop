<?php
include 'files/includes.php';

echo Document::getHeader();

$aProduct1 = ProductDao::getOne( "MXbajry0tmS" );
$aProduct2 = ProductDao::getOne( "VMYVyHgCY0s" );

?>


    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center hero-background">
        <div class="product-device box-shadow d-none d-md-block"></div>
        <div class="product-device product-device-2 box-shadow d-none d-md-block"></div>
    </div>

    <div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
        <a class="no_style" href="/product/<?php echo $aProduct1[ 'ID' ] ?>">
            <div class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden">
                <div class="my-3 py-3">
                    <h2 class="display-5"><?php echo $aProduct1[ 'Name' ] ?></h2>
                    <p class="lead"><?php echo $aProduct1[ 'Description' ] ?></p>
                </div>
                <div class="bg-light box-shadow mx-auto"
                     style="width: 80%; height: 300px; border-radius: 21px 21px 0 0; background: url('<?php echo $aProduct1[ 'Image' ] ?>') center center; background-size: cover;"></div>
            </div>
        </a>
        <a class="no_style" href="/product/<?php echo $aProduct2[ 'ID' ] ?>">
            <div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
                <div class="my-3 p-3">
                    <h2 class="display-5"><?php echo $aProduct2[ 'Name' ] ?></h2>
                    <p class="lead"><?php echo $aProduct2[ 'Description' ] ?></p>
                </div>
                <div class="bg-dark box-shadow mx-auto"
                     style="width: 80%; height: 300px; border-radius: 21px 21px 0 0; background: url('<?php echo $aProduct2[ 'Image' ] ?>') center center; background-size: cover;"></div>
            </div>
        </a>
    </div>


<?php
echo Document::getFooter();
