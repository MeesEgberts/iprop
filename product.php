<?php
include 'files/includes.php';

echo Document::getHeader();

$sProductID = $_GET[ 'id' ];
if ( ! $aProduct = ProductDao::getOne( $sProductID ) ) {
	echo 'This product cannot be found';
	exit;
}
if ( isset( $_POST[ 'quantity' ] ) ) {
	OrderDao::create( $sProductID, $_POST[ 'quantity' ], $aProduct[ 'SupplierID' ], $_POST[ 'mail' ] );
}

?>

    <div class="container">
        <div class="row">
            <div class="col-sm">
                <img src="<?php echo $aProduct[ 'Image' ] ?>" alt="Mooie img">
            </div>
            <div class="col-sm">
                <div class="information">
                    <div class="information-content">
                        <h1>
							<?php echo $aProduct[ 'Name' ] ?>
                        </h1>
                        <h2>
                            &euro; <?php echo Format::showPrice( $aProduct[ 'Prijs' ] ) ?>
                        </h2>
                        <p>
							<?php echo $aProduct[ 'Description' ] ?>
                        </p>
                        <small style="color: red">Bezorg tijd: <b><?php echo $aProduct[ 'DeliveryTime' ] ?></b> dagen</small>
                        <br>
                        <small style="color: red">Beschikbare items: <b><?php echo $aProduct[ 'Stock' ] ?></b></small>
                        <div class="order">
                            <form method="post">
                                <label for="quantity">Hoeveelheid:</label>
                                <input type="number" placeholder="1" value="1" id="quantity" name="quantity" max="<?php echo $aProduct[ 'Stock' ] ?>" autocomplete="off" required>
                                <br>
                                <label for="mail">Email</label>
                                <input type="email" placeholder="Email" id="mail" name="mail" autocomplete="off" required>
                                <hr>
                                <button id="order_button" type="submit">
                                    Bestellen
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
echo Document::getFooter();