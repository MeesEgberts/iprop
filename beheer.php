<?php

include 'files/includes.php';

echo Document::getHeader();

if (isset($_POST['order_id'])) {
	OrderDao::updateStatus($_POST['order_id'], $_POST['status']);
}

$aOrders = OrderDao::getAll();
$aLeveranciers = SupplierDao::getAll();

?>


	<div class="container">
		<div class="row">
			<h1>
				Bestellingen
			</h1>
			<table>
				<tr>
					<th>Item</th>
					<th>Hoeveelheid</th>
					<th>Status</th>
				</tr>
				<?php
				foreach ($aOrders as $order) {
					$aProduct = ProductDao::getOne($order['ProductID']);

					echo '<form method="post"><tr><input type="hidden" name="order_id" value="' . $order['ID'] . '">';
					echo '<td>' . $aProduct['Name'] . '</td>';
					echo '<td>' . $order['Quantity'] . '</td>';
					if ($order['Status'] === 'in behandeling') {
						echo '<td><select name="status" id="" onchange="this.form.submit()">
										<option value="In behandeling" selected>In behandeling</option>
										<option value="Verzonden">Verzonden</option>
									</select></td>';
					} else if ($order['Status'] === 'verzonden') {
						echo '<td><select name="status" id="" onchange="this.form.submit()">
										<option value="In behandeling">In behandeling</option>
										<option value="Verzonden" selected>Verzonden</option>
									</select></td>';
					}

					echo '</tr></form>';
				}
				?>
			</table>
		</div>
		<h1>
			Leveranciers
		</h1>
		<?php
		foreach ($aLeveranciers as $leverancier) {
			echo '<div class="row">';
			$aproducts = SupplierDao::getProducts($leverancier['ID']);
			echo '<h2>' . $leverancier['Name'] . '</h2>';
			echo '
		<table>
			<tr>
				<th>Item</th>
				<th>Voorraad</th>
				<th>Bezorg tijd (in dagen)</th>
			</tr>';
			foreach ($aproducts as $product) {
				echo '
				<tr>
					<td>' . $product['Name'] . '</td>
					<td>' . $product['Stock'] . '</td>
					<td>' . $product['DeliveryTime'] . '</td>
				</tr>
				';
			}
			echo '</table></div>';
		} ?>
	</div>


<?php
echo Document::getFooter();
