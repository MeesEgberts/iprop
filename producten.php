<?php
include 'files/includes.php';

echo Document::getHeader();

$aProducten = ProductDao::getAll();
$productsRows = array_chunk($aProducten, 2);
?>

<?php
$i = 0;
foreach ($productsRows as $productsRow) {
	echo '<div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">';
	foreach ($productsRow as $product) {
		$i++;

		if ($i % 2) {
			echo '
		<a class="no_style" href="/product/' . $product['ID'] . '">
			<div class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden">
				<div class="my-3 py-3">
					<h2 class="display-5">' . $product['Name'] . '</h2>
					<p class="lead">' . $product['Description'] . '</p>
				</div>
				<div class="bg-light box-shadow mx-auto"
				     style="width: 80%; height: 300px; border-radius: 21px 21px 0 0; background: url(' . $product['Image'] . ') center center; background-size: cover;"></div>
			</div>
		</a>
		';
		} else {
			echo '
		<a class="no_style" href="/product/' . $product['ID'] . '">
			<div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
				<div class="my-3 py-3">
					<h2 class="display-5">' . $product['Name'] . '</h2>
					<p class="lead">' . $product['Description'] . '</p>
				</div>
				<div class="bg-dark box-shadow mx-auto"
				     style="width: 80%; height: 300px; border-radius: 21px 21px 0 0; background: url(' . $product['Image'] . ') center center; background-size: cover;"></div>
			</div>
		</a>
		';
		}

	}
	echo '</div>';

}
?>


<?php
echo Document::getFooter();
